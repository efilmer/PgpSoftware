//////////////////////////////////////////////////////////////////////////////
// This file is part of 'Example RCE Project'.
// It is subject to the license terms in the LICENSE.txt file found in the 
// top-level directory of this distribution and at: 
//    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
// No part of 'Example RCE Project', including this file, 
// may be copied, modified, propagated, or distributed except according to 
// the terms contained in the LICENSE.txt file.
//////////////////////////////////////////////////////////////////////////////
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h> 
#include <Pgp2bAxiReg.h>

int main (int argc, char **argv) {

   RceRegister rce;
   std::cout << "Build Stamp: "<< rce.buildString() << std::endl << std::endl;
   rce.dumpRegs();
   return(0);
}
