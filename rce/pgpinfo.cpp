//////////////////////////////////////////////////////////////////////////////
// This file is part of 'Example RCE Project'.
// It is subject to the license terms in the LICENSE.txt file found in the 
// top-level directory of this distribution and at: 
//    https://confluence.slac.stanford.edu/display/ppareg/LICENSE.html. 
// No part of 'Example RCE Project', including this file, 
// may be copied, modified, propagated, or distributed except according to 
// the terms contained in the LICENSE.txt file.
//////////////////////////////////////////////////////////////////////////////
#include <Pgp2bAxiReg.h>
#include <iostream>
int main (int argc, char **argv) {
  if(argc!=2) {
    std::cerr << "usage: " <<  "pgpinfo <PGP lane (0-11)>" << std::endl;
    return (-1);
  }
  unsigned int lane=-1;
  try {
    lane=std::stoi(argv[1]);
  }
  catch(...) {    
  }
  if(lane<0 || lane >11) {
    std::cerr << "pgpinfo: Invalid argument: " << argv[1] <<std::endl;
    return(-2);
  }

  PgpRegister pgp(lane);
  pgp.dumpRegs();
  return(0);
}
